package options

import "github.com/spf13/pflag"

type FeatureOptions struct {
	EnableProfiling bool `json:"prof" mapstructure:"prof"`
	EnableMetrics   bool `json:"metrics" mapstructure:"metrics"`
}

func NewFeatureOptsion() *FeatureOptions {
	return &FeatureOptions{
		EnableProfiling: false,
		EnableMetrics:   true,
	}
}

func (o *FeatureOptions) Validate() []error {
	return []error{}
}

func (o *FeatureOptions) AddFlags(fs *pflag.FlagSet) {
	if fs == nil {
		return
	}

	fs.BoolVar(&o.EnableProfiling, "feature.prof", o.EnableProfiling, "Enable profiling web interface host:port/debug/pprof/")
	fs.BoolVar(&o.EnableMetrics, "feature.metrics", o.EnableMetrics, "Enables metrics on server at /metrics")
}
