package options

import (
	"github.com/spf13/pflag"
)

type GRPCOptions struct {
	Address    string `json:"address" mapstructure:"address"`
	MaxMsgSize int    `json:"max-msg-size" mapstructure:"max-msg-size"`
	CertFile   string `json:"cert-file" mapstructure:"cert-file"`
	KeyFile    string `json:"private-key-file" mapstructure:"private-key-file"`
}

func NewGRPCOptions() *GRPCOptions {
	return &GRPCOptions{
		Address:    "0.0.0.0:8081",
		MaxMsgSize: 4 * 1024 * 1024,
	}
}

func (s *GRPCOptions) Validate() []error {
	errs := []error{}

	return errs
}

func (s *GRPCOptions) AddFlags(fs *pflag.FlagSet) {
	fs.StringVar(&s.Address, "grpc.address", s.Address, ""+
		"The IP address on which to serve the (set to 0.0.0.0 for all IPv4 interfaces and :: for all IPv6 interfaces).")

	fs.IntVar(&s.MaxMsgSize, "grpc.max-msg-size", s.MaxMsgSize, "gRPC max message size.")

	fs.StringVar(&s.CertFile, "grpc.cert-file", s.CertFile, ""+
		"File containing the default x509 Certificate for GRPC. (CA cert, if any, concatenated "+
		"after server cert).")

	fs.StringVar(&s.KeyFile, "grpc.private-key-file",
		s.KeyFile, ""+
			"File containing the default x509 private key matching --grpc.cert-file.")
}

type GRPCClientOptions struct {
	Address  string `json:"rpcserver" mapstructure:"rpcserver"`
	ClientCA string `json:"client-ca-file" mapstructure:"client-ca-file"`
}

func NewGRPCClientOptions() *GRPCClientOptions {
	return &GRPCClientOptions{
		Address: "127.0.0.1:8081",
	}
}

func (s *GRPCClientOptions) Validate() []error {
	errs := []error{}

	return errs
}

func (s *GRPCClientOptions) AddFlags(fs *pflag.FlagSet) {
	fs.StringVar(&s.Address, "grpc.rpcserver", s.Address, "The address of tge rpc server. "+
		"The rpc server can provide all the secrets and policies to use.")

	fs.StringVar(&s.ClientCA, "grpc.client-ca-file", s.ClientCA, ""+
		"If set, any request presenting a client certificate signed by one of "+
		"the authorities in the client-ca-file is authenticated with an identity "+
		"corresponding to the CommonName of the client certificate.")

}
