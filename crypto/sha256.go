package crypto

import (
	"crypto/sha256"
	"encoding/hex"
)

func GetSHA256FromByte(src []byte) string {
	hash := sha256.New()
	hash.Write(src)
	return hex.EncodeToString(hash.Sum(nil))
}

func GetSHA256FromString(src string) string {
	hash := sha256.New()
	hash.Write([]byte(src))
	return hex.EncodeToString(hash.Sum(nil))
}
