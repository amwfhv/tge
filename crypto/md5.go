package crypto

import (
	"crypto/md5"
	"encoding/hex"
)

func GetMD5FromByte(src []byte) string {
	m := md5.New()
	m.Write(src)
	return hex.EncodeToString(m.Sum(nil))
}

func GetMD5FromString(src string) string {
	m := md5.New()
	m.Write([]byte(src))
	return hex.EncodeToString(m.Sum(nil))
}
