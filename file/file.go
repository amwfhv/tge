package file

import "os"

// Exist 判断文件是否存在
func Exist(name string) bool {
	_, err := os.Stat(name)
	return !os.IsNotExist(err)
}
