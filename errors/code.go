package errors

import (
	"fmt"
	"net/http"
	"sync"
)

var (
	unknownCoder defaultCoder = defaultCoder{1, http.StatusInternalServerError, "An internal server error occurred", "http://gitee.com/amwfhv/tge/errors/README.md"}
)

type Coder interface {
	// 错误信息
	String() string

	// Http状态码
	HTTPStatus() int
	// 错误参考文档
	Reference() string

	// 错误码
	Code() int
}

type defaultCoder struct {
	// ErrorCode
	c int
	// httpstatus
	http int
	// 错误信息
	msg string
	// 错误参考
	ref string
}

func (coder defaultCoder) Code() int {
	return coder.c
}

func (coder defaultCoder) HTTPStatus() int {
	return coder.http
}

func (coder defaultCoder) String() string {
	return coder.msg
}

func (coder defaultCoder) Reference() string {
	return coder.ref
}

var codes = map[int]Coder{}
var codeMux = &sync.Mutex{}

func Register(coder Coder) {
	if coder.Code() == 0 {
		panic("code `0` is reserved by `gitee.com/amwfhv/tge/errors` as unknownCode error code")
	}

	codeMux.Lock()
	defer codeMux.Unlock()

	codes[coder.Code()] = coder
}

func MustRegister(coder Coder) {
	if coder.Code() == 0 {
		panic("code '0' is reserved by 'gitee.com/amwfhv/tge/errors' as ErrUnknown error code")
	}

	codeMux.Lock()
	defer codeMux.Unlock()

	if _, ok := codes[coder.Code()]; ok {
		panic(fmt.Sprintf("code: %d already exist", coder.Code()))
	}

	codes[coder.Code()] = coder
}

func ParseCoder(err error) Coder {
	if err == nil {
		return nil
	}

	if v, ok := err.(*withCode); ok {
		if coder, ok := codes[v.code]; ok {
			return coder
		}
	}

	return unknownCoder
}

func IsCode(err error, code int) bool {
	if v, ok := err.(*withCode); ok {
		if v.code == code {
			return true
		}

		if v.cause != nil {
			return IsCode(v.cause, code)
		}

		return false
	}

	return false
}

func init() {
	codes[unknownCoder.Code()] = unknownCoder
}
